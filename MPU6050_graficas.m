close all;
clear all;
clc;

oldobj = instrfind; %eleminiar infomacion presente en el puerto serial
if not(isempty(oldobj))
    fclose(oldobj);
    delete(oldobj);
end

if ~exist('s','var')
    s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if ~isvalid(s)
    s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
end
if strcmp(get(s,'status'),'closed')
    fopen(s);
end


SENSITIVITY_ACCEL = 2.0/32768.0;
SENSITIVITY_GYRO = 250.0/32968.0;


disp('Posiciones el sensor en la posición inicial');
pause(); %preciones cualquier tecla

disp('Cominezo')
fprintf(s,'H');
i=1;
while(1) %lee los datos en el tiempo determinado por la stm
    str{i}= fscanf(s);
    if(str{i}(1)== 'A')
        disp('fin');
        break;
    end
    i=i+1;
end
fclose(s);
n = length(str)-1;
c=1;
for i=1:n
    temp= cellfun(@str2num,strsplit(str{i},',')); %Selecciona un string para separarlo psoteriormente
    if numel(temp)==8
        values(i,:)=temp;
    end
end
offset=[mean(values)]

save Hola values
%_________________________FIGURAS______________________________________

Nsamples=length(values);
dt=0.01;
t=0:dt:Nsamples*dt-dt;
%___________________________________________
% Acelerómetro RAW
%___________________________________________
figure;
plot(t,values(:,3)*SENSITIVITY_ACCEL,'r')%ax
hold on
plot(t,values(:,4)*SENSITIVITY_ACCEL,'g')%ay
plot(t,values(:,5)*SENSITIVITY_ACCEL,'b')%az
title('Acelerometró de MPU6050 sin calibrar')
ylabel('Aceleración (g)')
xlabel('Tiempo (segundos)')
legend('ax','ay','az','Location', 'northeast','Orientation','horizontal')

%___________________________________________
% Acelerómetro calibrado
%___________________________________________
figure;
plot(t,(values(:,3)-offset(3))*SENSITIVITY_ACCEL,'r')%ax
hold on
plot(t,(values(:,4)-offset(4))*SENSITIVITY_ACCEL,'g')%ay
plot(t,(values(:,5)-(offset(5)-32768/2))*SENSITIVITY_ACCEL,'b')%az
title('Acelerometró de MPU6050 calibrado')
ylabel('Aceleración (g)')
xlabel('Tiempo (segundos)')
legend('ax','ay','az','Location', 'northeast','Orientation','horizontal')

%___________________________________________
% Giroscopio RAW
%___________________________________________
figure;
plot(t,values(:,6)*SENSITIVITY_GYRO,'r')%ax
hold on
plot(t,values(:,7)*SENSITIVITY_GYRO,'g')%ay
plot(t,values(:,8)*SENSITIVITY_GYRO,'b')%az
title('Giroscópio de MPU6050 sin calibrar')
ylabel('Velocidad angular (°/s)')
xlabel('Tiempo (segundos)')
legend('ax','ay','az','Location', 'northeast','Orientation','horizontal')

%___________________________________________
% Giroscopio Calibrado
%___________________________________________
figure;
plot(t,(values(:,6)-offset(6))*SENSITIVITY_GYRO,'r')%ax
hold on
plot(t,(values(:,7)-offset(7))*SENSITIVITY_GYRO,'g')%ay
plot(t,(values(:,8)-offset(8))*SENSITIVITY_GYRO,'b')%az
title('Giroscópio de MPU6050 calibrado')
ylabel('Velocidad angular (°/s)')
xlabel('Tiempo (segundos)')
legend('ax','ay','az','Location', 'northeast','Orientation','horizontal')
